const gameContainer = document.getElementById("game");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);


// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);

    // call a function handleCardClick when a div is clicked on

    newDiv.addEventListener("click", handleCardClick);


    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}
let count = 0;
maxNumOfClicks = 2
let arr = []
let temp;
let numOfClicks = 0;
let winner = 0;
let bug = []
// TODO: Implement this function!
// if(count)

function handleCardClick(event) {
  numOfClicks++;
  console.log(numOfClicks + " clicks")
  if (numOfClicks > 1) {
    let disableEvents = gameContainer.children
    Array.from(disableEvents).forEach(function (element) {
      element.removeEventListener(event.type, handleCardClick)
    });
  }
  console.log(bug)
  console.log("Hello")
  console.log("you clicked on this div -->>", event.target);
  let boxColor = event.target;
  boxColor.style.backgroundColor = boxColor.className
  event.target.removeEventListener(event.type, arguments.callee);
  count++;
  if (count === 1) {
    temp = boxColor;
  }

  console.log(count, maxNumOfClicks)
  arr.push(boxColor.className)
  if (count === maxNumOfClicks) {
    if (arr[0] === arr[1]) {
      console.log("It is a match")
      setTimeout(function () {   
        bug.push(temp)    
        bug.push(boxColor) 
        let enableEvents = gameContainer.children
        Array.from(enableEvents).forEach(function (element) {
          element.addEventListener(event.type, handleCardClick)
        });
        temp.removeEventListener("click", handleCardClick)
        boxColor.removeEventListener("click", handleCardClick)
        numOfClicks = 0
      }, 1000);
      winner++;
      if (winner === 5) {
        alert("Game Over !!!")
      }
      console.log("winner=" + winner)
    }
    else {
      setTimeout(function () {        
        console.log("Not match");
        temp.style.backgroundColor = "white"
        boxColor.style.backgroundColor = "white"
        let enableEvents = gameContainer.children
        Array.from(enableEvents).forEach(function (element) {
          element.addEventListener(event.type, handleCardClick)
        });
        Array.from(bug).forEach(function (element) {
          element.removeEventListener(event.type, handleCardClick)
        });
        console.log(bug)
        numOfClicks = 0
        // temp.addEventListener("click", handleCardClick) 
        // boxColor.addEventListener("click", handleCardClick)      
      }, 1000);
    }
    arr = []
  }
  if (count === 2)
    count = 0;
}

// when the DOM loads
createDivsForColors(shuffledColors);
