const gameContainer = document.getElementById("game");
gameContainer.style.display = "none"
const startButton = document.getElementById("startGame")
const bestScore = document.getElementById("bestScore")
const liveScore = document.getElementById("liveScore")
const restartButton = document.getElementById("restartGame")
const mainHeading=document.querySelector("h1")
const bodyBackgroundColor=document.querySelector("body")
restartButton.style.display = "none"
startButton.addEventListener("click", display)
function display(e) {
  e.target.style.display = "none";
  mainHeading.style.display = "none";
  gameContainer.style.display = "block"  
  restartButton.style.display = "block"
  restartButton.addEventListener("click", warningMsg)
  function warningMsg(event) {
   let refreshResult=confirm("Are you sure you want to restart the game !!!")
   if(refreshResult===true)   
   window.location.reload();
  }
  liveScore.innerHTML = '<h1>Live Score: 0</h1>';
  if (localStorage.getItem('Best Score') === null) {
    bestScore.innerHTML = '<h1>Best Score: 0</h1>';
  }
  else {
    bestScore.innerHTML = '<h1>' + "Best Score: " + localStorage.getItem("Best Score") + '</h1>';
  }
}

const COLORS = [
  "img1",
  "img2",
  "img3",
  "img4",
  "img5",
  "img6",
  "img7",
  "img8",
  "img9",
  "img10",
  "img11",
  "img12",
  "img1",
  "img2",
  "img3",
  "img4",
  "img5",
  "img6",
  "img7",
  "img8",
  "img9",
  "img10",
  "img11",
  "img12",
];

function shuffle(array) {
  let counter = array.length;
  
  while (counter > 0) {
    
    let index = Math.floor(Math.random() * counter);
    counter--;
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }
  return array;
}

let shuffledColors = shuffle(COLORS);
let newDivScore;
let newDivBestScore;

function createDivsForColors(colorArray) { 
  for (let color of colorArray) {   
    const newDiv = document.createElement("div");   
    newDiv.classList.add(color);
    newDiv.style.backgroundImage = "url('./displayPicture.gif')"
    newDiv.style.backgroundSize = "cover";
    newDiv.style.border="3px solid white"
    newDiv.style.borderRadius="3em"
    newDiv.addEventListener("click", handleCardClick);
    gameContainer.append(newDiv);
  }
}
let boxCount = 0;
let maxNumOfClicks = 2
let gifArray = []
let previouslyClickedBox;
let numOfClicks = 0;
let winner = 0;
let matchingBoxesArray = []
let score = 0;

function handleCardClick(event) {

  numOfClicks++;
  if (numOfClicks > 1) {
    let disableEvents = gameContainer.children
    Array.from(disableEvents).forEach(function (element) {
      element.removeEventListener(event.type, handleCardClick)
    });
  }
  let firstBox = event.target;
  let imgClass = event.target.className
  if (imgClass !== "score")
  firstBox.style.backgroundImage = "url('./gifs/" + imgClass + ".gif')";
  firstBox.style.backgroundSize = "cover";
  event.target.removeEventListener(event.type, arguments.callee);
  boxCount++;
  if (boxCount === 1) {
    previouslyClickedBox = firstBox;
  }

  gifArray.push(firstBox.style.backgroundImage)
  if (boxCount === maxNumOfClicks) {
    if (gifArray[0] === gifArray[1]) {
      console.log("It is a match")
      setTimeout(function () {
        matchingBoxesArray.push(previouslyClickedBox)
        matchingBoxesArray.push(firstBox)
        let enableEvents = gameContainer.children
        Array.from(enableEvents).forEach(function (element) {
          if (element.className !== "score")
            element.addEventListener(event.type, handleCardClick)
        });
        previouslyClickedBox.removeEventListener("click", handleCardClick)
        firstBox.removeEventListener("click", handleCardClick)
        numOfClicks = 0
      }, 1000);
      winner++;
      if (winner === 12) {
        alert("Game Over !!!")
        sessionStorage.setItem('Best Score', score + 1);
        let bestScore = sessionStorage.getItem('Best Score');

        if (localStorage.getItem('Best Score') === null) {
          localStorage.setItem('Best Score', score + 1)
        }
        else {
          let availableBestLocalScore = localStorage.getItem('Best Score');
          if (bestScore < availableBestLocalScore)
            localStorage.setItem('Best Score', score + 1)
        }
      }
      console.log("winner=" + winner)
    }
    else {
      setTimeout(function () {
        console.log("Not match");
        if (imgClass !== "score") {
          previouslyClickedBox.style.backgroundImage = "url('./displayPicture.gif')"          
          firstBox.style.backgroundImage = "url('./displayPicture.gif')"         
        }
        let enableEvents = gameContainer.children
        Array.from(enableEvents).forEach(function (element) {
          if (element.className !== "score")
            element.addEventListener(event.type, handleCardClick)
        });
        Array.from(matchingBoxesArray).forEach(function (element) {
          element.removeEventListener(event.type, handleCardClick)
        });
        numOfClicks = 0
      }, 1000);
    }
    gifArray = []
  }
  if (boxCount === 2)
  boxCount = 0;
  score++;
  liveScore.innerHTML = '<h1>Live Score: '+ score+'</h1>'
  liveScore.style.fontSize = "2em";
  liveScore.style.fontWeight = "bolder";
}

createDivsForColors(shuffledColors);
